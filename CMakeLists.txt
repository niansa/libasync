cmake_minimum_required(VERSION 3.5)

project(libasync LANGUAGES C CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_subdirectory(frigg)



add_library(async INTERFACE)
target_link_libraries(async PUBLIC INTERFACE frigg)
target_include_directories(async INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}/include)
